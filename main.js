var bgRect = new Path.Rectangle({
    point: [0, 0],
    size: [view.size.width, view.size.height],
    strokeColor: 'white',
    selected: true
});

bgRect.sendToBack();
bgRect.fillColor = "#48392A";

var boxSize = [600, 300];
var box = new Path.Rectangle({
    point: [view.size.width/2 - boxSize[0]/2, view.size.height/2 - boxSize[1]/2],
    size: boxSize,
    strokeColor: '#fff'
});

var bolts = [];
var fd = 90;
var fdcur = fd;
var gameOver = false;
var spawnN = 10;
var failed = false;
var mPosX, mPosY, txt;

function spawn(n, fdm) {
    for (var i = 0; i < n; i++) {
        var lightning = new Shape.Rectangle({
            point: [Math.random() * ((view.size.width/2 - boxSize[0]/2 + boxSize[0]) - (view.size.width/2 - boxSize[0]/2)) + (view.size.width/2 - boxSize[0]/2),
                    Math.random() * ((view.size.height/2 - boxSize[1]/2 + boxSize[1]) - (view.size.height/2 - boxSize[1]/2)) + (view.size.height/2 - boxSize[1]/2)],
            size: [200, 15]
        })

        lightning.fillColor = "#FBF2C0";
        lightning.postype = 1;
        lightning.len = 200;
        lightning.fd = fdcur;
        lightning.fc = 0;
        lightning.dead = false;
        fdcur += fdm;

        bolts.push(lightning)
    }
}

spawn(spawnN, fd);

function fail() {
    txt = new PointText(new Point(view.size.width/2, view.size.height/2));
    txt.justification = 'center';
    txt.fillColor = 'red';
    txt.content = 'FAIL'
    txt.style = {
        fontSize: 100,
        fontFamily: 'monospace'
    };
    gameOver = true;
    failed = true;
}

function pass() {
    txt = new PointText(new Point(view.size.width/2, view.size.height/2));
    txt.justification = 'center';
    txt.fillColor = 'green';
    txt.content = 'PASS'
    txt.style = {
        fontSize: 100,
        fontFamily: 'monospace'
    };
    gameOver = true;
}

function onFrame() {
    if (!gameOver) {
        if (bolts.length == 0) {
            pass();
            gameOver = true;
        }

        bolts.forEach(function (lightning) {
            if (lightning.bounds.bottomRight.y < view.size.height && lightning.bounds.topRight.y > 0) {
                if (lightning.bounds.topRight.x < view.size.width && lightning.bounds.topLeft.x > 0) {
                    lightning.fc += 1;
                    if (lightning.fc > lightning.fd) {
                        lightning.len -= 5;
                        lightning.size = [lightning.len, 15];
                    }
                } else {
                    fail();
                }
            } else {
                fail();
            }
        });
    }
}

function onMouseDown(e) {
    if (!gameOver) {
        bolts.forEach(function (lightning, i, obj) {
            if (e.point.x > lightning.bounds.topLeft.x && e.point.x < lightning.bounds.topRight.x) {
                if (e.point.y > lightning.bounds.topLeft.y && e.point.y < lightning.bounds.bottomLeft.y) {
                    if (lightning.fc > lightning.fd) {
                        lightning.remove();
                        obj.splice(i, 1);
                    }
                }
            }
        });
    } else if (!failed) {
        fd *= 0.80;
        fdcur = fd;
        spawnN *= 1.5;
        txt.remove();
        spawn(spawnN, fd);
        gameOver = false;
    } else {
        for (var i = 0; i < 10; i++) {
            setTimeout(function () {
                spawn(spawnN, fd);
            }, 250 * i);
        }
        setTimeout(function () {
            location.reload();
        }, 3500);
    }
}

function onMouseMove(e) {
    mPosX = e.point.x;
    mPosY = e.point.y;
}

function onKeyDown(e) {
    if (e.key == 'g' || e.key.toLowerCase() == 'space') {
        if (!gameOver) {
            bolts.forEach(function (lightning, i, obj) {
                if (mPosX > lightning.bounds.topLeft.x && mPosX < lightning.bounds.topRight.x) {
                    if (mPosY > lightning.bounds.topLeft.y && mPosY < lightning.bounds.bottomLeft.y) {
                        if (lightning.fc > lightning.fd) {
                            lightning.remove();
                            obj.splice(i, 1);
                        }
                    }
                }
            });
        }
    }
}